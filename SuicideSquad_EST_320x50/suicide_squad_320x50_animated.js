(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 320,
	height: 50,
	fps: 24,
	color: "#86E86B",
	opacity: 1.00,
	manifest: [
		{src:"images/bg.png?1477178519268", id:"bg"},
		{src:"images/date_a.png?1477178519268", id:"date_a"},
		{src:"images/legal.png?1477178519268", id:"legal"},
		{src:"images/replay.png?1477178519268", id:"replay"},
		{src:"images/title_a.png?1477178519268", id:"title_a"},
		{src:"images/title_b.png?1477178519268", id:"title_b"},
		{src:"images/title_c.png?1477178519268", id:"title_c"},
		{src:"images/title_d.png?1477178519268", id:"title_d"}
	]
};



lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,320,100);


(lib.date_a = function() {
	this.initialize(img.date_a);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.legal = function() {
	this.initialize(img.legal);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.replay = function() {
	this.initialize(img.replay);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,24,24);


(lib.title_a = function() {
	this.initialize(img.title_a);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.title_b = function() {
	this.initialize(img.title_b);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.title_c = function() {
	this.initialize(img.title_c);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.title_d = function() {
	this.initialize(img.title_d);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.WHITE = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A4/D6IAAnzMAx/AAAIAAHzg");
	this.shape.setTransform(160,25);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.rewind = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// btn
	this.instance = new lib.replay();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,24,24);


(lib.mc_title4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.title_d();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.mc_title_c = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.title_c();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.mc_title_b = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.title_b();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.mc_title_a = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.title_a();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.mc_legal = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.legal();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.mc_date = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.date_a();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,320,50);


(lib.mc_bg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.bg();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,320,100);


(lib.clicktag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0066CC").s().p("A4/D6IAAnzMAx/AAAIAAHzg");
	this.shape.setTransform(160,25);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.mc_cont = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_39 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(1));

    // legal
	this.instance_5 = new lib.mc_legal();
	this.instance_5.setTransform(150,125,1,1,0,0,0,150,125);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(16).to({_off:false},0).to({alpha:1},18,cjs.Ease.get(1)).wait(6));
    
	// Layer 2
	this.instance = new lib.mc_title4();
	this.instance.setTransform(70.5,36.6,2,2,10,0,0,70.5,36.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,scaleY:1,rotation:0,y:36.5},6).wait(28));

	// title_b
	this.instance_1 = new lib.mc_title_b();
	this.instance_1.setTransform(114.4,16.4,2,2,14,0,0,114.3,16.4);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(3).to({_off:false},0).to({regY:16.5,scaleX:1,scaleY:1,rotation:0,x:114.3,y:16.5},6,cjs.Ease.get(1)).wait(31));

	// title_a
	this.instance_2 = new lib.mc_title_a();
	this.instance_2.setTransform(42.1,15.6,2,2,-18,0,0,42,15.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX:1,scaleY:1,rotation:0,x:42,y:15.5},6,cjs.Ease.get(1)).wait(34));

	// date
	this.instance_3 = new lib.mc_date();
	this.instance_3.setTransform(234.2,20.2,2,2,0,0,0,234.1,20.1);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(9).to({_off:false},0).to({scaleX:1,scaleY:1,x:234.1,y:20.1,alpha:1},6,cjs.Ease.get(1)).wait(25));

	// title_c
	this.instance_4 = new lib.mc_title_c();
	this.instance_4.setTransform(70.5,36.5,0.25,0.25,-24,0,0,70.6,36.7);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({regX:70.5,regY:36.5,scaleX:1,scaleY:1,rotation:0,alpha:1},6,cjs.Ease.get(1)).wait(28));

	

	// bg
	this.instance_6 = new lib.mc_bg();
	this.instance_6.setTransform(150,175,1,1,0,0,0,150,175);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({y:125},39).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-47.4,-185.7,639.6,292.9);


// stage content:



(lib.suicide_squad_320x50_animated = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		//var cta_bg = this.mc_cont.mc_cta.mc_cta_bg;
		
		
		//function dateCheck() {
		//	var d1 = new Date();
		//	var d2 = new Date();
		//	d1.setFullYear(2016, 10, 15);
		//	d2.setFullYear(2016, 11, 6);
		//	var now = new Date();
		//	if (now < d1) {
		//		console.log("PRE PST");
		//		//    cta_bg.y = 0;
		//		date_a = "images/date_b.png"
		//		//gwd.actions.events.getElementById("div_hp_date").setAttribute("source", "hp_date_a.png");
		//		//  gwd.actions.events.getElementById("div_ep_date").setAttribute("source", "ep_date_a.png");
		//	} else if (now >= d1 && now < d2) {
		
		//		console.log("POST PST");
		
		//	} else if (now >= d2) {
		//		console.log(" PST");
		
		//		//date_image = "images/date_b.png"
		//		//gwd.actions.events.getElementById("div_hp_date").setAttribute("source", "hp_date_b.png");
		//		//gwd.actions.events.getElementById("div_ep_date").setAttribute("source", "ep_date_b.png");
		//	}
		//}
		//dateCheck()
		var rolled = false;
		var clicked = false;
		var myTO;
		
		//turns mouse pointer into hand
		this.ct_click.cursor = "pointer";
		
		//listens for click event and calls click function
		this.ct_click.addEventListener("click", clickDC);
		
		//click function
		function clickDC() {
			//console.log("Clicktag1")
			//	gotoAndPlay(240);
		 //window.open(clickTag, "_blank");
		Enabler.exit("clickTag");
		}
		
		
		//this.ct_click.addEventListener("click", fl_ClickToGoToAndStopAtFrame.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame() {
			this.gotoAndStop(39);
		this.mc_cont.gotoAndStop(39);
		
			if (rolled == true) {
				clearTimeout(myTO);
			}
			if (clicked == false) {
				clicked = true;
				rolled = true;
				// gwd.actions.gwdDoubleclick.incrementCounter('gwd-ad', 'WBH### : RM Interactive Impression', false);
		
				Enabler.counter('WBH### : RM Interactive Impression');
		
			}
		
		}
		
		this.rewindBtn.cursor = "pointer";
		
		//this.rewindBtn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame_4.bind(this));
		
		function fl_ClickToGoToAndPlayFromFrame_4() {
		
			Enabler.counter('WBH### : RM Interactive Impression');
		
			this.mc_cont.gotoAndPlay(2);
			this.gotoAndPlay(2);
		
			rolled = true;
			clicked = true;
		
		}
		
		
		
		
		var frequency = 3;
		stage.enableMouseOver(frequency);
		this.ct_click.addEventListener("mouseover", fl_MouseOverHandler);
		
		function fl_MouseOverHandler() {
			if (rolled == false) {
				rolled = true;
				myTO = setTimeout(function () {
					// gwd.actions.gwdDoubleclick.incrementCounter('gwd-ad', 'WBH### : RM Interactive Impression', false);
		
					Enabler.counter('WBH### : RM Interactive Impression');
		
					clicked = true;
				}, 3000);
			}
		}
	}
	this.frame_39 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(39).call(this.frame_39).wait(1));

	// replay
	this.rewindBtn = new lib.rewind();
	this.rewindBtn.setTransform(302.2,-19.7,1,1,0,0,0,6.2,6.3);
	this.rewindBtn.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.rewindBtn).wait(40));

	// clicktag
	this.ct_click = new lib.clicktag();
	this.ct_click.setTransform(80,300,1,1,0,0,0,80,300);
	new cjs.ButtonHelper(this.ct_click, 0, 1, 2, false, new lib.clicktag(), 3);

	this.timeline.addTween(cjs.Tween.get(this.ct_click).wait(40));

	// stroke
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,0,3).p("A46j0MAx1AAAIAAHpMgx1AAAg");
	this.shape.setTransform(160,25);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(40));

	// WHITE
	this.instance = new lib.WHITE();
	this.instance.setTransform(150,125,1,1,0,0,0,150,125);
	this.instance.compositeOperation = "lighter";

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:0},5,cjs.Ease.get(1)).to({_off:true},1).wait(34));

	// cont
	this.mc_cont = new lib.mc_cont();
	this.mc_cont.setTransform(150,125,1,1,0,0,0,150,125);

	this.timeline.addTween(cjs.Tween.get(this.mc_cont).wait(5).to({x:149,y:124},0).wait(1).to({x:150,y:123},0).wait(1).to({x:151,y:124},0).wait(1).to({x:150,y:125},0).wait(1).to({x:149,y:124},0).wait(1).to({x:151,y:126},0).wait(1).to({x:149},0).wait(1).to({x:151,y:124},0).wait(1).to({x:149},0).wait(1).to({x:151,y:126},0).wait(1).to({x:149},0).wait(1).to({x:151,y:124},0).wait(2).to({x:149},0).wait(1).to({x:150,y:125},0).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(112.6,-160.7,639.6,292.9);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;