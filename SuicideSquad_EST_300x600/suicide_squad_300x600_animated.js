(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 300,
	height: 600,
	fps: 24,
	color: "#86E86B",
	opacity: 1.00,
	manifest: [
		{src:"images/bg.png?1477172706201", id:"bg"},
		{src:"images/date_a.png?1477172706201", id:"date_a"},
		{src:"images/deadshot.png?1477172706201", id:"deadshot"},
		{src:"images/harley.png?1477172706201", id:"harley"},
		{src:"images/joker.png?1477172706201", id:"joker"},
		{src:"images/legal.png?1477172706201", id:"legal"},
		{src:"images/replay.png?1477172706201", id:"replay"},
		{src:"images/title_a.png?1477172706201", id:"title_a"},
		{src:"images/title_b.png?1477172706201", id:"title_b"},
		{src:"images/title_c.png?1477172706201", id:"title_c"}
	]
};



lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,700);


(lib.date_a = function() {
	this.initialize(img.date_a);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.deadshot = function() {
	this.initialize(img.deadshot);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.harley = function() {
	this.initialize(img.harley);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.joker = function() {
	this.initialize(img.joker);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.legal = function() {
	this.initialize(img.legal);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.replay = function() {
	this.initialize(img.replay);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,24,24);


(lib.title_a = function() {
	this.initialize(img.title_a);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.title_b = function() {
	this.initialize(img.title_b);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.title_c = function() {
	this.initialize(img.title_c);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.WHITE = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgXbAu4MAAAhdvMAu2AAAMAAABdvg");
	this.shape.setTransform(150,300);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.rewind = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// btn
	this.instance = new lib.replay();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,24,24);


(lib.mc_title_c = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.title_c();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.mc_title_b = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.title_b();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.mc_title_a = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.title_a();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.mc_legal = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.legal();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.mc_joker = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.joker();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.mc_harley = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.harley();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.mc_deadshot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.deadshot();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.mc_date = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.date_a();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.mc_bg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.bg();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,700);


(lib.clicktag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0066CC").s().p("EgXbAu4MAAAhdvMAu2AAAMAAABdvg");
	this.shape.setTransform(150,300);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.mc_cont = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_131 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(131).call(this.frame_131).wait(1));

	// title_b
	this.instance = new lib.mc_title_b();
	this.instance.setTransform(147,123.2,2,2,23,0,0,147,123.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off:false},0).to({scaleX:1,scaleY:1,rotation:0,y:123.1},6,cjs.Ease.get(1)).wait(123));

	// title_a
	this.instance_1 = new lib.mc_title_a();
	this.instance_1.setTransform(147.2,50.1,2,2,-24,0,0,147.1,50.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regY:50,scaleX:1,scaleY:1,rotation:0,x:147.1,y:50},6,cjs.Ease.get(1)).wait(126));

	// date
	this.instance_2 = new lib.mc_date();
	this.instance_2.setTransform(103.1,291.7,2,2,0,0,0,103,291.6);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(9).to({_off:false},0).to({scaleX:1,scaleY:1,x:103,y:291.6,alpha:1},6,cjs.Ease.get(1)).wait(117));

	// joker
	this.instance_3 = new lib.mc_joker();
	this.instance_3.setTransform(300,312.1,0.25,0.25,-30,0,0,260.4,322.4);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(20).to({_off:false},0).to({regX:260,regY:322.1,scaleX:0.9,scaleY:0.9,rotation:-19,x:293,y:332.1},6,cjs.Ease.get(1)).to({scaleX:1,scaleY:1,rotation:0,x:260},105).wait(1));

	// title_c
	this.instance_4 = new lib.mc_title_c();
	this.instance_4.setTransform(150,189,0.25,0.25,-17,0,0,149.9,189.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({regX:150,regY:189,scaleX:1,scaleY:1,rotation:0},6,cjs.Ease.get(1)).wait(120));

	// legal
	this.instance_5 = new lib.mc_legal();
	this.instance_5.setTransform(150,125,1,1,0,0,0,150,125);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(38).to({_off:false},0).to({alpha:1},18,cjs.Ease.get(1)).wait(76));

	// deadshot
	this.instance_6 = new lib.mc_deadshot();
	this.instance_6.setTransform(147.1,580.1,0.25,0.25,37,0,0,146.9,480.1);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(17).to({_off:false},0).to({regX:147.2,regY:480.3,scaleX:0.9,scaleY:0.9,rotation:11,x:147,y:497.1},6,cjs.Ease.get(1)).to({regX:147,regY:480.1,scaleX:1,scaleY:1,rotation:0,y:480.1},104).wait(5));

	// harley
	this.instance_7 = new lib.mc_harley();
	this.instance_7.setTransform(45,579.2,0.25,0.25,-36,0,0,84,488.1);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(14).to({_off:false},0).to({regX:80,regY:485.2,scaleX:0.9,scaleY:0.9,rotation:-15,x:80,y:515.2},6,cjs.Ease.get(1)).to({regY:485.1,scaleX:1,scaleY:1,rotation:0,y:485.1},100).wait(12));

	// bg
	this.instance_8 = new lib.mc_bg();
	this.instance_8.setTransform(150,175,1,1,0,0,0,150,175);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({y:75},131).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-162.3,-165.8,1036.3,1340.3);


// stage content:



(lib.suicide_squad_300x600_animated = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		//var cta_bg = this.mc_cont.mc_cta.mc_cta_bg;
		
		
		//function dateCheck() {
		//	var d1 = new Date();
		//	var d2 = new Date();
		//	d1.setFullYear(2016, 10, 15);
		//	d2.setFullYear(2016, 11, 6);
		//	var now = new Date();
		//	if (now < d1) {
		//		console.log("PRE PST");
		//		//    cta_bg.y = 0;
		//		date_a = "images/date_b.png"
		//		//gwd.actions.events.getElementById("div_hp_date").setAttribute("source", "hp_date_a.png");
		//		//  gwd.actions.events.getElementById("div_ep_date").setAttribute("source", "ep_date_a.png");
		//	} else if (now >= d1 && now < d2) {
		
		//		console.log("POST PST");
		
		//	} else if (now >= d2) {
		//		console.log(" PST");
		
		//		//date_image = "images/date_b.png"
		//		//gwd.actions.events.getElementById("div_hp_date").setAttribute("source", "hp_date_b.png");
		//		//gwd.actions.events.getElementById("div_ep_date").setAttribute("source", "ep_date_b.png");
		//	}
		//}
		//dateCheck()
		var rolled = false;
		var clicked = false;
		var myTO;
		
		//turns mouse pointer into hand
		this.ct_click.cursor = "pointer";
		
		//listens for click event and calls click function
		this.ct_click.addEventListener("click", clickDC);
		
		//click function
		function clickDC() {
			//console.log("Clicktag1")
			//	gotoAndPlay(240);
					//	gotoAndPlay(240);
	      //window.open(clickTag, "_blank");
		  Enabler.exit("clickTag");
		}
		
		
		//this.ct_click.addEventListener("click", fl_ClickToGoToAndStopAtFrame.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame() {
			this.gotoAndStop(138);
		this.mc_cont.gotoAndStop(131);
		
			if (rolled == true) {
				clearTimeout(myTO);
			}
			if (clicked == false) {
				clicked = true;
				rolled = true;
				// gwd.actions.gwdDoubleclick.incrementCounter('gwd-ad', 'WBH### : RM Interactive Impression', false);
		
				Enabler.counter('WBH### : RM Interactive Impression');
		
			}
		
		}
		
		this.rewindBtn.cursor = "pointer";
		
		//this.rewindBtn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame_4.bind(this));
		
		function fl_ClickToGoToAndPlayFromFrame_4() {
		
			Enabler.counter('WBH### : RM Interactive Impression');
		
			this.mc_cont.gotoAndPlay(2);
			this.gotoAndPlay(2);
		
			rolled = true;
			clicked = true;
		
		}
		
		
		
		
		var frequency = 3;
		stage.enableMouseOver(frequency);
		this.ct_click.addEventListener("mouseover", fl_MouseOverHandler);
		
		function fl_MouseOverHandler() {
			if (rolled == false) {
				rolled = true;
				myTO = setTimeout(function () {
					// gwd.actions.gwdDoubleclick.incrementCounter('gwd-ad', 'WBH### : RM Interactive Impression', false);
		
					Enabler.counter('WBH### : RM Interactive Impression');
		
					clicked = true;
				}, 3000);
			}
		}
	}
	this.frame_138 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(138).call(this.frame_138).wait(1));

	// replay
	this.rewindBtn = new lib.rewind();
	this.rewindBtn.setTransform(282.2,-21.7,1,1,0,0,0,6.2,6.3);
	this.rewindBtn.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.rewindBtn).wait(121).to({y:6.3},0).to({alpha:1},8).wait(10));

	// clicktag
	this.ct_click = new lib.clicktag();
	this.ct_click.setTransform(80,300,1,1,0,0,0,80,300);
	new cjs.ButtonHelper(this.ct_click, 0, 1, 2, false, new lib.clicktag(), 3);

	this.timeline.addTween(cjs.Tween.get(this.ct_click).wait(139));

	// stroke
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,0,3).p("EgXWguyMAutAAAMAAABdlMgutAAAg");
	this.shape.setTransform(150,300);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(139));

	// WHITE
	this.instance = new lib.WHITE();
	this.instance.setTransform(150,125,1,1,0,0,0,150,125);
	this.instance.compositeOperation = "lighter";

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:0},5,cjs.Ease.get(1)).to({_off:true},1).wait(133));

	// cont
	this.mc_cont = new lib.mc_cont();
	this.mc_cont.setTransform(150,125,1,1,0,0,0,150,125);

	this.timeline.addTween(cjs.Tween.get(this.mc_cont).wait(5).to({x:149,y:124},0).wait(1).to({x:150,y:123},0).wait(1).to({x:151,y:124},0).wait(1).to({x:150,y:125},0).wait(1).to({x:149,y:124},0).wait(1).to({x:151,y:126},0).wait(1).to({x:149},0).wait(1).to({x:151,y:124},0).wait(1).to({x:149},0).wait(1).to({x:151,y:126},0).wait(1).to({x:149},0).wait(1).to({x:151,y:124},0).wait(2).to({x:149},0).wait(1).to({x:151,y:126},0).wait(1).to({x:149},0).wait(1).to({x:151,y:124},0).wait(1).to({x:149},0).wait(1).to({x:151,y:126},0).wait(1).to({x:149},0).wait(1).to({x:151,y:124},0).wait(1).to({x:150,y:125},0).wait(113));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.3,134.2,1036.3,1340.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;