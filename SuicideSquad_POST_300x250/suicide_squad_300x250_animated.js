(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 300,
	height: 250,
	fps: 24,
	color: "#86E86B",
	opacity: 1.00,
	manifest: [
		{src:"images/bg.png?1477171660527", id:"bg"},
		{src:"images/cta_bg.png?1477171660527", id:"cta_bg"},
		{src:"images/date_a.png?1477171660528", id:"date_a"},
		{src:"images/deadshot.png?1477171660528", id:"deadshot"},
		{src:"images/harley.png?1477171660528", id:"harley"},
		{src:"images/joker.png?1477171660528", id:"joker"},
		{src:"images/legal.png?1477171660528", id:"legal"},
		{src:"images/replay.png?1477171660528", id:"replay"},
		{src:"images/title_a.png?1477171660528", id:"title_a"},
		{src:"images/title_b.png?1477171660528", id:"title_b"},
		{src:"images/title_c.png?1477171660528", id:"title_c"}
	]
};



lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,350);


(lib.cta_bg = function() {
	this.initialize(img.cta_bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,48);


(lib.date_a = function() {
	this.initialize(img.date_a);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.deadshot = function() {
	this.initialize(img.deadshot);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.harley = function() {
	this.initialize(img.harley);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.joker = function() {
	this.initialize(img.joker);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.legal = function() {
	this.initialize(img.legal);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.replay = function() {
	this.initialize(img.replay);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);


(lib.title_a = function() {
	this.initialize(img.title_a);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.title_b = function() {
	this.initialize(img.title_b);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.title_c = function() {
	this.initialize(img.title_c);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.WHITE = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A3bTiMAAAgnDMAu2AAAMAAAAnDg");
	this.shape.setTransform(150,125);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.rewind = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// btn
	this.instance = new lib.replay();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,24,24);


(lib.mc_title_c = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.title_c();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.mc_title_b = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.title_b();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.mc_title_a = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.title_a();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.mc_legal = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.legal();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.mc_joker = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.joker();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.mc_harley = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.harley();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.mc_deadshot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.deadshot();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.mc_date = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.date_a();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.mc_cta_bg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.cta_bg();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,43);


(lib.mc_bg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.bg();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,350);


(lib.clicktag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0066CC").s().p("A3bTiMAAAgnDMAu2AAAMAAAAnDg");
	this.shape.setTransform(150,125);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.mc_cta = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.mc_cta_bg = new lib.mc_cta_bg();
	this.mc_cta_bg.setTransform(150,16,1,1,0,0,0,150,16.5);

	this.timeline.addTween(cjs.Tween.get(this.mc_cta_bg).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,48);


(lib.mc_cont = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_131 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(131).call(this.frame_131).wait(1));

    // legal
	this.instance_5 = new lib.mc_legal();
	this.instance_5.setTransform(150,125,1,1,0,0,0,150,125);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(38).to({_off:false},0).to({alpha:1},18,cjs.Ease.get(1)).wait(76));
    
	// title_b
	this.instance = new lib.mc_title_b();
	this.instance.setTransform(202,24,2,2,17,0,0,202,24);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off:false},0).to({scaleX:1,scaleY:1,rotation:0},6,cjs.Ease.get(1)).wait(123));

	// title_a
	this.instance_1 = new lib.mc_title_a();
	this.instance_1.setTransform(90.1,35,2,2,-18,0,0,90,35);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:1,scaleY:1,rotation:0,x:90},6,cjs.Ease.get(1)).wait(126));

	// date
	this.instance_2 = new lib.mc_date();
	this.instance_2.setTransform(150,231.5,2,2,0,0,0,150,231.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(9).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},6,cjs.Ease.get(1)).wait(117));

	// cta_bg
	this.mc_cta = new lib.mc_cta();
	this.mc_cta.setTransform(150,271.5,1,1,0,0,0,150,21.5);

	this.timeline.addTween(cjs.Tween.get(this.mc_cta).wait(5).to({y:236.5},7,cjs.Ease.get(1)).wait(120));

	// joker
	this.instance_3 = new lib.mc_joker();
	this.instance_3.setTransform(299,125,0.25,0.25,-16,0,0,260,125.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(20).to({_off:false},0).to({regX:260.1,regY:125,scaleX:0.9,scaleY:0.9,rotation:-12,x:279.1},6,cjs.Ease.get(1)).to({regX:260,scaleX:1,scaleY:1,rotation:0,x:260},105).wait(1));

	// title_c
	this.instance_4 = new lib.mc_title_c();
	this.instance_4.setTransform(122,71,0.25,0.25,-11,0,0,122.1,71);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({regX:122,scaleX:1,scaleY:1,rotation:0},6,cjs.Ease.get(1)).wait(120));

	

	// deadshot
	this.instance_6 = new lib.mc_deadshot();
	this.instance_6.setTransform(137,210,0.25,0.25,18,0,0,137,180.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(17).to({_off:false},0).to({regX:136.9,regY:180.1,scaleX:0.9,scaleY:0.9,rotation:14,y:180},6,cjs.Ease.get(1)).to({regX:137,regY:180,scaleX:1,scaleY:1,rotation:0},104).wait(5));

	// harley
	this.instance_7 = new lib.mc_harley();
	this.instance_7.setTransform(43,226,0.25,0.25,-35,0,0,52.9,186.7);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(14).to({_off:false},0).to({regX:53.1,regY:186.1,scaleX:0.9,scaleY:0.9,rotation:-18,x:53.1,y:186},6,cjs.Ease.get(1)).to({regX:53,regY:186,scaleX:1,scaleY:1,rotation:0,x:53},100).wait(12));

	// bg
	this.instance_8 = new lib.mc_bg();
	this.instance_8.setTransform(150,175,1,1,0,0,0,150,175);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({y:75},131).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-102.8,-161.3,725.2,660.9);


// stage content:



(lib.suicide_squad_300x250_animated = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		//var cta_bg = this.mc_cont.mc_cta.mc_cta_bg;
		
		
		//function dateCheck() {
		//	var d1 = new Date();
		//	var d2 = new Date();
		//	d1.setFullYear(2016, 10, 15);
		//	d2.setFullYear(2016, 11, 6);
		//	var now = new Date();
		//	if (now < d1) {
		//		console.log("PRE PST");
		//		//    cta_bg.y = 0;
		//		date_a = "images/date_b.png"
		//		//gwd.actions.events.getElementById("div_hp_date").setAttribute("source", "hp_date_a.png");
		//		//  gwd.actions.events.getElementById("div_ep_date").setAttribute("source", "ep_date_a.png");
		//	} else if (now >= d1 && now < d2) {
		
		//		console.log("POST PST");
		
		//	} else if (now >= d2) {
		//		console.log(" PST");
		
		//		//date_image = "images/date_b.png"
		//		//gwd.actions.events.getElementById("div_hp_date").setAttribute("source", "hp_date_b.png");
		//		//gwd.actions.events.getElementById("div_ep_date").setAttribute("source", "ep_date_b.png");
		//	}
		//}
		//dateCheck()
		var rolled = false;
		var clicked = false;
		var myTO;
		
		//turns mouse pointer into hand
		this.ct_click.cursor = "pointer";
		
		//listens for click event and calls click function
		this.ct_click.addEventListener("click", clickDC);
		
		//click function
		function clickDC() {
			//console.log("Clicktag1")
			//	gotoAndPlay(240);
			//Enabler.exit("clickTag");
            //window.open(clickTag, "_blank");
            Enabler.exit("clickTag");
		
		}
		
		
		
		function fl_ClickToGoToAndStopAtFrame() {
			this.gotoAndStop(138);
		this.mc_cont.gotoAndStop(131);
		
			if (rolled == true) {
				clearTimeout(myTO);
			}
			if (clicked == false) {
				clicked = true;
				rolled = true;
				// gwd.actions.gwdDoubleclick.incrementCounter('gwd-ad', 'WBH### : RM Interactive Impression', false);
		
				Enabler.counter('WBH### : RM Interactive Impression');
		
			}
		
		}
		
		this.rewindBtn.cursor = "pointer";
		
		
		function fl_ClickToGoToAndPlayFromFrame_4() {
		
			Enabler.counter('WBH### : RM Interactive Impression');
		
			this.mc_cont.gotoAndPlay(2);
			this.gotoAndPlay(2);
		
			rolled = true;
			clicked = true;
		
		}
		
		
		
		
		var frequency = 3;
		stage.enableMouseOver(frequency);
		
		function fl_MouseOverHandler() {
			if (rolled == false) {
				rolled = true;
				myTO = setTimeout(function () {
					// gwd.actions.gwdDoubleclick.incrementCounter('gwd-ad', 'WBH### : RM Interactive Impression', false);
		
					Enabler.counter('WBH### : RM Interactive Impression');
		
					clicked = true;
				}, 3000);
			}
		}
	}
	this.frame_138 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(138).call(this.frame_138).wait(1));

	// replay
	this.rewindBtn = new lib.rewind();
	this.rewindBtn.setTransform(282.2,-21.7,1,1,0,0,0,6.2,6.3);
	this.rewindBtn.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.rewindBtn).wait(121).to({y:6.3},0).to({alpha:1},8).wait(10));

	// clicktag
	this.ct_click = new lib.clicktag();
	this.ct_click.setTransform(80,300,1,1,0,0,0,80,300);
	new cjs.ButtonHelper(this.ct_click, 0, 1, 2, false, new lib.clicktag(), 3);

	this.timeline.addTween(cjs.Tween.get(this.ct_click).wait(139));

	// stroke
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,0,3).p("A3WzcMAutAAAMAAAAm5MgutAAAg");
	this.shape.setTransform(150,125);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(139));

	// WHITE
	this.instance = new lib.WHITE();
	this.instance.setTransform(150,125,1,1,0,0,0,150,125);
	this.instance.compositeOperation = "lighter";

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:0},5,cjs.Ease.get(1)).to({_off:true},1).wait(133));

	// cont
	this.mc_cont = new lib.mc_cont();
	this.mc_cont.setTransform(150,125,1,1,0,0,0,150,125);

	this.timeline.addTween(cjs.Tween.get(this.mc_cont).wait(5).to({x:149,y:124},0).wait(1).to({x:150,y:123},0).wait(1).to({x:151,y:124},0).wait(1).to({x:150,y:125},0).wait(1).to({x:149,y:124},0).wait(1).to({x:151,y:126},0).wait(1).to({x:149},0).wait(1).to({x:151,y:124},0).wait(1).to({x:149},0).wait(1).to({x:151,y:126},0).wait(1).to({x:149},0).wait(1).to({x:151,y:124},0).wait(2).to({x:149},0).wait(1).to({x:151,y:126},0).wait(1).to({x:149},0).wait(1).to({x:151,y:124},0).wait(1).to({x:149},0).wait(1).to({x:151,y:126},0).wait(1).to({x:149},0).wait(1).to({x:151,y:124},0).wait(1).to({x:150,y:125},0).wait(113));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(47.2,-36.3,725.2,660.9);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;